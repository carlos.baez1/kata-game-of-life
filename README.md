# GAME OF LIFE

time estimation: 2 hours more or less

For the evaluation, it will keep in mind:
- Test and coverage.
- Good design and naming.
- Documentation.


You can choose the language that you prefer. We would appreciate if you attach: 
- Decision and design document (it can be small).
- Installation and execution steps.


## STEP 1 (GAME OF LIFE)

Game of life implementation.

### EXPLANATION

The universe of the [Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) is an infinite two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, alive or dead, or "populated" or "unpopulated". Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent. At each step in time, the following transitions occur:

- Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
- Any live cell with two or three live neighbours lives on to the next generation.
- Any live cell with more than three live neighbours dies, as if by overpopulation.
- Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

The initial pattern constitutes the seed of the system. The first generation is created by applying the above rules simultaneously to every cell in the seed—births and deaths occur simultaneously, and the discrete moment at which this happens is sometimes called a tick (in other words, each generation is a pure function of the preceding one). The rules continue to be applied repeatedly to create further generations.

You should write a program that can accept an arbitrary grid of cells, and will output a similar grid showing the next generation.

For this step, you must implement an API library which all these features (previous 4 rules). Please think about our guidelines for the development. This library should have entrypoint to autogenerate 
games.

Suggested Test Cases Make sure you have enough coverage of edge cases - where there are births and deaths at the edge of the grid.


> $ gameoflife input_text_file number_of_generations

- gameoffile name of your program
- input text file
```
     4 8
     ........
     ....*...
     ...**...
     ........
```


- number of cicles or generations that they must execute.

One example:

```
$ gamefile generation1.txt 1
........
...**...
...**...
........
```


## STEP 2 (RULESTRINGS)


Now, we want to include new features in the code to work with [rulestrings](https://www.conwaylife.com/wiki/Rulestring). It permits a customization of rules in the game. In our case, we will use
the *B/S notation* to define rules: 

>  B{number list}/S{number list};.  B (for birth) is a list of all the numbers of live neighbors that cause a dead cell to come alive (be born); S (for survival) is a list of all the numbers of live neighbors that cause a live cell to remain alive (survive).

This new modification implies a change in the input of our program. 


> $ gameoflife input_text_file number_of_generations 

- gameoffile name of your program
- input text file
```
     B3/S2 
     4 8
     ........
     ....*...
     ...**...
     ........
```


## STEP 3 (INFINITIVE MAP)
In this case, We remove the grid map and it will be  an infinitive map. It means, we don t need grid specification and we only use seed positions.

 > $ gameoflife input_text_file number_of_generations 

- gameoffile name of your program
- input text file
```
     B3/S2 
     1,5
     2,4
     2,5 
```

- seeds represent some thing like this:
     ........
     ....*...
     ...**...
     ........



## STEP 4

Ther idea for this step is the improvement of the code to avoid some bad practices and keep a clean code. To do this, we have done a list of mandatory constraints (some of them can not be applied. )


#### Not primitives across method boundaries

> The constraint “No primitives across method boundaries” means that, separate from the constructor (which isn’t really a ‘normal’ method), you cannot have a method that returns or is passed a primitive (int, string etc.). You have to use objects. So this way you’re forced to take the Primitive Obsession code smell seriously, and use more objects.

#### Not conditional statements:

- No ifs, no switch, no loops for conditional. The alternatives are polymorphism and hashtables
- Conditional statements are a form of primitive obsession (we tend to use the lowest level of abstraction instead of choosing a higher level). For example, we may choose an int for an employee number instead of an employee number class.

#### Immutable can be simpler than mutable

#### Value object to avoid primitive obsession

> A Value object is created by encapsulating the primitive inside an object with a name (Pins in this example) and making sure that two objects with the same value are equal (normally by overriding the equals method).

#### Polymorphism to control state

#### Only four lines per method



## EXTRA POSSIBLE ACTIVITIES

- Change partner every 10 mins
- Ping Pong: One person writes the test, the other implements the test.
- Taking baby steps. 5 mins write code, 5 mins test and 5 mins refactor, if they don't finish before the timer goes off they have to delete and start over. 






